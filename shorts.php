<?php
/*
Template Name: Shorts
*/
?>
<?php get_header(); ?>
<?php // include (TEMPLATEPATH . '/lib/inc/grid-directors.php' ); ?>

	<?php
		if ( is_page( 'directors' ) ) {  //grab a list of directors ?>
			<article>
				<h2 class="clearfix"><?php the_title(); ?></h2>
				<ul id="" class="director-list">
					<?php wp_list_pages("title_li=&child_of=7");?>
				</ul>
			</article>
		<?php } else
			if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<div style="position: relative;">
					<div style="position: absolute; top:10px; left: 810px;" class="pageTitle"><?php the_title(); ?></div>

</div>

					<div class="entry work" style="margin-left:30px; clear: both; padding-top:40px;">
						<?php the_content(); ?>

<div id="videoWrapper" style="height:1px; overflow: hidden; width:900px;">
<div id="videoPlayer" style="text-align: center; margin-bottom: 20px;"></div>
</div>

<div class="galleryContainer" id="imgGal<?php echo $i; ?>" style="display: inline;">

<?php $i = 1; ?>
<?php $attachments = new Attachments( 'attachments' ); /* pass the instance name */ ?>
<?php if( $attachments->exist() ) : ?>
    <?php while( $attachments->get() ) : ?>
	
	

	
		<div onclick="javascript:changeIt2(videoVar<?php echo $i ?>);" style="cursor: hand; cursor: pointer; position: relative; width: 295px; height: 181px; float: left; margin-right: 10px; margin-top: 10px;">

<div id="directorOverlay2">
<?php echo $attachments->field( 'title' ); ?>
</div>

<img src="<?php echo $attachments->src( 'full' ); ?>" style="" width="295" height="181"></div>

		<script language="javascript" type="text/javascript">
			
			<?php $string1 = htmlspecialchars_decode($attachments->field( 'caption' )); ?>
			videoVar<?php echo $i ?> = '<?php echo $string1; ?>';
	
		</script>
        	<!--<?php echo $attachments->field( 'caption' ); ?>-->
	
	<?php $i = $i+1; ?>
    <?php endwhile; ?>
<?php endif; ?>


</div>

<div style="clear: both;"></div>

					</div>

		<!--			<footer class="postmetadata">
		    				<?php the_tags('Tags: ', ', ', '<br />'); ?>
		    				Posted in <?php the_category(', ') ?> |
		    				<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
		    			</footer> -->

				</article>

			<?php endwhile; ?>


			<?php else : ?>

				<h2>Not Found</h2>

			<?php endif; ?>


	<!-- <?php include (TEMPLATEPATH . '/lib/inc/home-footer.php' ); ?> -->

<?php get_footer(); ?>