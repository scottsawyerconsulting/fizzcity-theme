<?php
/*
Template Name: directors
*/
?>
<?php get_header(); ?>
<div class="director-wrapper container">
<?php include (TEMPLATEPATH . '/lib/inc/grid-directors.php' );
	if ( is_page( 'directors' ) ) {  //grab a list of directors ?>
		<article>
			<h2 class="dir-name"><?php the_title(); ?></h2>
			<ul id="" class="director-list">
				<?php wp_list_pages("title_li=&child_of=7");?>
			</ul>
		</article>
		<?php } else
			if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2 class="dir-name"><?php the_title(); ?></h2>
					<div class="entry work">
						<?php the_content(); ?>
						
						<div class="galleryContainer" id="imgGal<?php echo $i; ?>">
							<?php $i = 1; ?>
							<?php $attachments = new Attachments( 'attachments' ); /* pass the instance name */ ?>
							<?php if( $attachments->exist() ) : ?>
    							<?php while( $attachments->get() ) : ?>
								<div class="gridSingle" onclick="javascript:changeIt(videoVar<?php echo $i ?>);">
									<div id="directorOverlay">
										<?php echo $attachments->field( 'title' ); ?>
									</div>
									<img class="directorThumbs" id="rover" src="<?php echo $attachments->src( 'full' ); ?>" style=""></div>
										<script language="javascript" type="text/javascript">
											<?php $string1 = htmlspecialchars_decode($attachments->field( 'caption' )); ?>
											videoVar<?php echo $i ?> = '<?php echo $string1; ?>';
									</script>
        							<!--<?php echo $attachments->field( 'caption' ); ?>-->
									<?php $i = $i+1; ?>
    							<?php endwhile; ?>
							<?php endif; ?>
								</div>
								<div style="clear: both;"></div>
						</div>
						<!--<footer class="postmetadata">
						<?php the_tags('Tags: ', ', ', '<br />'); ?>
						Posted in <?php the_category(', ') ?> |
						<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
						</footer> -->
				</article>
			<?php endwhile; ?>
		<?php else : ?>
			h2>Not Found</h2>
		<?php endif; ?>


	<!-- <?php include (TEMPLATEPATH . '/lib/inc/home-footer.php' ); ?> -->
</div>
<?php get_footer(); ?>