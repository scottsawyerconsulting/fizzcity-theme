<div id="" class="home-footer">
	<?php query_posts( 'p=78' ); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
			<section class="post_content clearfix">
				<?php the_content(); ?>
			</section> <!-- end article section -->
		</article> <!-- end article -->

		<?php endwhile; ?>
		<?php else : ?>
		<article id="post-not-found">
		    <header>
			<h1>Not Found</h1>
		    </header>
		    <section class="post_content">
			<p>Sorry, but the requested resource was not found on this site.</p>
		    </section>
		    <footer>
		    </footer>
		</article>

		<?php endif; ?>
	<?php wp_reset_query(); ?> <!-- Reset Query -->

</div><!-- / -->