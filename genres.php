<?php
/*
Template Name: Digital
*/
?>
<?php get_header(); ?>
<div class="genres-wrapper container">
<?php // include (TEMPLATEPATH . '/lib/inc/grid-directors.php' ); ?>

	<?php
		if ( is_page( 'directors' ) ) {  //grab a list of directors ?>
			<article>
				<h2 class="clearfix"><?php the_title(); ?></h2>
				<ul id="" class="director-list">
					<?php wp_list_pages("title_li=&child_of=7");?>
				</ul>
			</article>
		<?php } else
			if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<div style="position: relative;">
					<div class="pageTitle"><?php the_title(); ?></div>

</div>

					<div class="entry work" style="clear:both;">
						<?php the_content(); ?>

<div id="videoWrapper">
<span class="close-modal-genre">CLOSE&nbsp;&nbsp;X</span>
<div id="videoPlayer" style="text-align: center; "></div>
</div>

<div class="galleryContainer" id="imgGal">

<?php $i = 1; ?>
<?php $attachments = new Attachments( 'attachments' ); /* pass the instance name */ ?>
<?php if( $attachments->exist() ) : ?>
    <?php while( $attachments->get() ) : ?>
	
	

	
		<div class="gallerySingle" onclick="javascript:changeIt2(videoVar<?php echo $i ?>);" style="">

<div id="directorOverlay2">
<?php echo $attachments->field( 'title' ); ?>
</div>

<img class="directorThumbs2" src="<?php echo $attachments->src( 'full' ); ?>" style="" width="295" height="181"></div>

		<script language="javascript" type="text/javascript">
			
			<?php $string1 = htmlspecialchars_decode($attachments->field( 'caption' )); ?>
			videoVar<?php echo $i ?> = '<?php echo $string1; ?>';
	
		</script>
        	<!--<?php echo $attachments->field( 'caption' ); ?>-->
	
	<?php $i = $i+1; ?>
    <?php endwhile; ?>
<?php endif; ?>


</div>

<div style="clear: both;"></div>

					</div>

		<!--			<footer class="postmetadata">
		    				<?php the_tags('Tags: ', ', ', '<br />'); ?>
		    				Posted in <?php the_category(', ') ?> |
		    				<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
		    			</footer> -->

				</article>

			<?php endwhile; ?>


			<?php else : ?>

				<h2>Not Found</h2>

			<?php endif; ?>


	<!-- <?php include (TEMPLATEPATH . '/lib/inc/home-footer.php' ); ?> -->
</div>
<?php get_footer(); ?>
