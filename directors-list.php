<?php
/*
Template Name: directors-list
*/
?>
<?php get_header(); ?>
<div class="directors-wrapper container">
<h2 class="pageTitle"><?php the_title(); ?></h2>
<div class="clearfix" style="clear:both;"></div>
<?php include (TEMPLATEPATH . '/lib/inc/grid-home.php' ); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">



<?php

/*$args = array(
	'sort_order' => 'ASC',
	'sort_column' => 'post_date'
);
*/
$args = array(
	'sort_column' => 'menu_order',
	'sort_order' => 'ASC'
	); 

$mypages = get_pages($args);
foreach ( $mypages as $mypage  ) {
if ( has_post_thumbnail($mypage->ID)) {
	
	$pID = $mypage->ID;
	$show1 = get_post_meta($pID, 'show_director', true);
	
	if ($show1==1){
	
      echo '<div class="home-featured"><a href="' . get_permalink( $mypage->ID ) . '" title="' . esc_attr( $mypage->post_title ) . '">';
      echo get_the_post_thumbnail($mypage->ID, 'full');
      echo '</a>';
      echo '<!-- ' . $mypage->menu_order . ' -->';
	$short1 = get_post_meta($pID, 'short_name', true);
      	echo '<div class="thumbNailText"><a href="'.get_permalink( $mypage->ID ).'">'.$short1.'</a></div>';
      echo '</div>';


	}
    }

}
?>



<div style="clear: both;"></div>

			<div class="entry">
				<?php the_content(); ?>
			</div>

<!--			<footer class="postmetadata">
    				<?php the_tags('Tags: ', ', ', '<br />'); ?>
    				Posted in <?php the_category(', ') ?> |
    				<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
    			</footer> -->

		</article>

	<?php endwhile; ?>


	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>

	<?php include (TEMPLATEPATH . '/lib/inc/footer-home.php' ); ?>
</div>
<?php get_footer(); ?>
