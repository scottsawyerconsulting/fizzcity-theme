

			<footer id="" class="footer source-org vcard copyright">
            <div class="woman-owned" style="float:right;clear:right;clear:left;display:block;margin-right:10px;">
<!--    <img width="90px" height="37px;" src="
/site-content/uploads/2014/10/WOB_REVERSED_STD_RGB.png" alt="Woman Owned Business"> -->
  </div>
				<small>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></small>
			</footer>

    </div><!-- /page-grid -->

		</div> <!-- page-wrap -->
	</div> <!-- page-bg -->

	<?php wp_footer(); ?>


<!-- here comes the javascript -->

<!-- jQuery is called via the Wordpress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/lib/js/functions.js"></script>

<!-- Asynchronous google analytics; this is the official snippet.
	 Replace UA-XXXXXX-XX with your site's ID and uncomment to enable.-->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30709063-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>

</html>



