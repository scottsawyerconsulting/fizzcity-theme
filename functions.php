<?php
		// Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'html5reset', TEMPLATEPATH . '/languages' );

		$locale = get_locale();
		$locale_file = TEMPLATEPATH . "/languages/$locale.php";
		if ( is_readable($locale_file) )
			require_once($locale_file);

	// Add RSS links to <head> section
	add_theme_support( 'automatic-feed-links' );

	// Load jQuery
	if ( !function_exists("core_mods") ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script('jquery');
				wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"), false);
				wp_register_script('swipebox', get_bloginfo('stylesheet_directory' ) . '/lib/js/jquery.swipebox.js' , true);

				wp_enqueue_script('jquery');
				wp_enqueue_script('swipebox');
			}
		}
	}
	add_action('wp_enqueue_scripts', 'core_mods');

	//Registering Main Menu -> Ben Bowen 11/11/15
	register_nav_menus( array(
		'main_menu' => 'Main Menu',
	) );

	// Clean up the <head>
	function removeHeadLinks() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}
	add_action('init', 'removeHeadLinks');
	remove_action('wp_head', 'wp_generator');

	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => __('Sidebar Widgets','html5reset' ),
			'id'   => 'sidebar-widgets',
			'description'   => __( 'These are widgets for the sidebar.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));
	}

	add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.
	add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

	//Create a homepage shortcode link
	function homeLink() {
		return home_url();
	}
	add_shortcode('home','homeLink');

	function directorCat(){
		return wp_list_categories('child_of=3&title_li=');
	}
	add_shortcode('director','directorCat');

		//Load jQuery
	if(!is_admin()) {
		add_action('wp_enqueue_scripts', function() {
			wp_deregister_script('jquery');
			wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"), false, '1.7.1');
			wp_enqueue_script('jquery');
		});
	}

	// Create custom taxonomies
	function create_my_taxonomies() {
		register_taxonomy(
			'genres',
			'post',
			array(
				'hierarchical' => false,
				'label'        => 'Genres',
				'query_var'    => true,
				'rewrite'      => array( 'slug' => 'genre')
			)
		);
	}

	add_action('init', 'create_my_taxonomies', 0);

	// add category nicenames in body and post class
	function category_id_class($classes) {
	    global $post;
	    foreach((get_the_category($post->ID)) as $category)
	        $classes[] = $category->category_nicename;
	        return $classes;
	}
	add_filter('post_class', 'category_id_class');
	// add_filter('body_class', 'category_id_class');

    add_theme_support( 'title-tag' );

?>
