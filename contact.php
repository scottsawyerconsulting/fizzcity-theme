<?php
/*
Template Name: contact
*/
?>
<?php get_header(); ?>

<div class="about-wrapper container">
	<div class="contact-bar">
		<div class="pageTitle">CONTACT</div>
		<address class="address">
			3201 Peachtree Corners Circle<br>
			Peachtree Corners, Georgia 30092
		</address>
		<img class="alignnone size-full wp-image-577" src="http://fizzcityfilms.com/site-content/uploads/arrow.png" alt="arrow" width="21" height="31" />
		<a class="map-it" href="https://www.google.com/maps/place/3201+Peachtree+Corners+Cir,+Norcross,+GA+30092/@33.941,-84.2488599,17z/data=!3m1!4b1!4m2!3m1!1s0x88f5a0c801b854b3:0x66440d0db8a50f83">
			Map It
		</a>
		<a class="phone" href="tel:4048746783">
			(404) 874-6783
		</a>
		<a class="email" href="mailto:info@fizzcityfilms.com">
			info@fizzcityfilms.com
		</a>
		<div class="cf-contact">
			<div class="contact-form">
				<?php echo do_shortcode('[contact-form-7 id="145" title="Contact us"]'); ?>
			</div>
		</div>
		<div style="color:white;clear: both; text-align: left; font-family: 'brandon-grotesque'; font-weight: 200; font-size: 40px; margin-bottom: 20px;">FOLLOW US</div>
		<div style="text-align: center;">
<div style="text-align: center;">

<a href="http://www.facebook.com/pages/Fizz-City-Films/173694972732911"><img class="size-full wp-image-1499 aligncenter" src="http://fizzcityfilms.com/site-content/uploads/2012/02/facebook-icon.png" alt="" width="26" height="26" /></a>

</div>
		</div>
	</div>
	<div class="about-bar">
		<div class="pageTitle">ABOUT</div>
		<div style=""></div>
		<p>Fizz City Films is an artist driven film production boutique specializing in commercials, branded content and new media.</p>
		<p>The company is newly located in the Peachtree Corners community of Atlanta. Fizz City was founded on the premise of bringing internationally renowned talent to the Southeast, drawing from co-founder, Mark Simon’s mainstream Hollywood roots. The company has grown rapidly in the Atlanta community, and into a national and international scope.</p>
	</div>
</div>
<?php get_footer(); ?>
