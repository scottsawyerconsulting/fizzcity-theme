<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="www-fizzcityfilms-com">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title>
		<?php if (function_exists('is_tag') && is_tag()) {
			single_tag_title('Tag Archive for &quot;'); echo '&quot; - ';
		} elseif (is_archive()) {
			wp_title(''); echo ' Archive - ';
		} elseif (is_search()) {
			echo 'Search for &quot;'.wp_specialchars($s).'&quot; - ';
		} elseif (!(is_404()) && (is_single()) || (is_page())) {
			wp_title(''); echo ' - ';
		} elseif (is_404()) {
			echo 'Not Found - ';
		}
		if (is_home()) {
			bloginfo('name'); echo ' - '; bloginfo('description');
		} else {
			bloginfo('name');
		}
		if ($paged > 1) {
			echo ' - page '. $paged;
		} ?>
	</title>

	<meta name="title" content="<?php
			  if (function_exists('is_tag') && is_tag()) {
				 single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
			  elseif (is_archive()) {
				 wp_title(''); echo ' Archive - '; }
			  elseif (is_search()) {
				 echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
			  elseif (!(is_404()) && (is_single()) || (is_page())) {
				 wp_title(''); echo ' - '; }
			  elseif (is_404()) {
				 echo 'Not Found - '; }
			  if (is_home()) {
				 bloginfo('name'); echo ' - '; bloginfo('description'); }
			  else {
				  bloginfo('name'); }
			  if ($paged>1) {
				 echo ' - page '. $paged; }
		   ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<meta name="google-site-verification" content="">
	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->

	<meta name="author" content="Your Name Here">
	<meta name="Copyright" content="Copyright Your Name Here 2011. All Rights Reserved.">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="Project Name">
	<meta name="DC.subject" content="What you're about.">
	<meta name="DC.creator" content="Who made this site.">

	<!--  Mobile Viewport meta tag
	j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
	device-width : Occupy full width of the screen in its current orientation
	initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
	maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width -->
	<!-- Uncomment to use; use thoughtfully!
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	-->

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/lib/img/favicon.ico">
	<!-- This is the traditional favicon.
		 - size: 16x16 or 32x32
		 - transparency is OK
		 - see wikipedia for info on browser support: http://mky.be/favicon/ -->

	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/lib/img/apple-touch-icon.png">
	<!-- The is the icon for iOS's Web Clip.
		 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
		 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
		 - Transparency is not recommended (iOS will put a black BG behind the icon) -->

	<!-- CSS: screen, mobile & print are all in the same file -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<!-- all our JS is at the bottom of the page, except for Modernizr. -->
	<script src="<?php bloginfo('template_directory'); ?>/lib/js/modernizr-2.5.2.min.js"></script>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

<script type="text/javascript" src="//use.typekit.net/mkt0grs.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<script language="javascript" type="text/javascript">

function changeIt(x) {
	document.getElementById('theVideo').innerHTML = x;
}

function changeIt2(x) {
	expand1();
	document.getElementById('videoPlayer').innerHTML = x;
}


function expand1() {

  $('#videoWrapper').animate({
    height: '560'
  }, 1000, function() {
    // Animation complete.
  });

}





// remap jQuery to $
// (function($){})(window.jQuery);

var loc = "work";

/* trigger when page is ready */
$(document).ready(function (){

	// Global Nav dropdown menus
	var dropdown = $('ul.dropdown li').has('ul');
		$(dropdown).children('a').addClass("icon icon-arrow");
		$(dropdown).hover(function(){
			$(this).addClass("hover");
			$('ul:first',this).fadeToggle('100');
		}, function(){
			$(this).removeClass("hover");
			$('ul:first',this).fadeToggle('100');
		});
	
	
	

	// Director page switch control
	$('.director-switcher li').on('click',function() {

	
	if (loc=="work"){
		loc = "bio";
		document.getElementsByClassName("galleryContainer")[0].style.display = "none";
	} else {
		loc = "work";
		document.getElementsByClassName("galleryContainer")[0].style.display = "inline";
	}


		$("iframe").each(function() {// used to stop the vimeo video
			this.contentWindow.postMessage('{ "method": "pause" }', "http://player.vimeo.com");
		});
		var $this = $(this);
		if ($this.hasClass('on')) {
			return;			
		} else{			
			$this.siblings().removeClass('on');
			$this.addClass('on');
			$this.closest('.entry').toggleClass('work').toggleClass('bio');
		}

	});

	//Genre page director dropdown

	// Add the director label and the current director
	$('<ul class="director-dropdown-label"></ul>').appendTo('.genre-title');
	// $('<ul></ul>', {
	//	class: 'director-dropdown-label'
	// }).appendTo('.genre-title');
	$('<li></li>', {
		text: 'View Director'
	}).appendTo('ul.director-dropdown-label');
	// $('<li></li>', {
	//	class: 'current-director icon icon-arrow'
	// })
	$('<li class="current-director icon icon-arrow"></li>')
		.appendTo('ul.director-dropdown-label')
		.toggle(function(){						// On rollover show the director list
			$(this).addClass("hover");			//need to make this class work
			$('ul:first',this).fadeToggle();
		}, function(){
			$(this).removeClass("hover");
			$('ul:first',this).fadeToggle();
		});

	// Get the list of directors for the current page
	var directorList = [];
		$('<ul class="director-dropdown"></ul>').appendTo('.current-director'); //insert the director dropdown select code
		// $('<ul></ul>', { //insert the director dropdown select code
		//	class: 'director-dropdown'
		// }).appendTo('.current-director');

	// Put the list in an UL and hide it by default
	$('.director-name').each(function(i) { //get the name of the director from the category listing
		var $this = $(this);
		directorList[i]=$this.data('director');
		$('<li></li>', {
			text: directorList[i]
		}).appendTo('ul.director-dropdown');

	});

	// Display the name of the default director
	$('<span></span>', {
		text: directorList[0]
		// class: 'dir-name'
	}).prependTo('.current-director').addClass('dir-name');
	// $('<span></span>', {
	//	text: directorList[0],
	//	class: 'dir-name'
	// }).prependTo('.current-director');


	// Show the video for the selected director, hide all others
	$('article.director-name').filter(':nth-child(n+3)').hide(); //hide all videos on page load
	$('.directed-by').hide();

	// Change the current director to the selected director
	$('.director-dropdown li').on('click', function() {
		var dirName = $(this).text();
		var dirClassName = dirName.toLowerCase();
		dirClassName = dirClassName.replace(' ','-');
		dirClassName = '.'+dirClassName;
		$('.dir-name').text(dirName);
		$('.director-name').hide();
		$(dirClassName).toggle();

		$("iframe").each(function() {// used to stop the vimeo video when hiding, required the api=1 to be added to the vimeo link to work
			this.contentWindow.postMessage('{ "method": "pause" }', "http://player.vimeo.com");
		});

	});








});


// $(function(){
// });



/* optional triggers

$(window).load(function() {

});

$(window).resize(function() {

});

*/



</script>


</head>

<body <?php body_class(); ?> style="background: url('/site-content/uploads/new_bg.jpg') no-repeat top center fixed;">
	<div id="page-bg" style="background: none;">

		<div id="page-wrap"><!-- not needed? up to you: http://camendesign.com/code/developpeurs_sans_frontieres -->

			<header id="header" style="height: 200px; padding-top: 30px;">
				<ul class="social-nav" style="margin-top: 80px;">
					<img src="/site-content/uploads/socmedia.png" width="61" height="26" usemap="#Map5" border="0" />
<map name="Map5" id="Map5">
  <area shape="rect" coords="-2,0,27,29" href="https://twitter.com/#!/fizzcityfilms" target="_blank" />
  <area shape="rect" coords="29,-3,61,33" href="http://www.facebook.com/pages/Fizz-City-Films/173694972732911" target="_blank" />
</map>

				

				</ul>

				<a href="<?php echo get_option('home'); ?>/" title="Home" style="margin-top: 20px; margin-left:26px;"><img src="/site-content/uploads/newlogo3.png"></a>

				<nav role="navigation" style="float: right; margin-right: -96px; margin-top: 47px;">
					<ul class="dropdown">
						<li class="page_item"><a href="/">COMMERCIAL</a></li>
						<li class="page_item"><a href="/digital/">DIGITAL</a></li>
						<li class="page_item"><a href="/about/">ABOUT US</a></li>

						<?php
							// global $id;
							// wp_list_pages("exclude=-76&title_li=&child_of=$id");
						?>
					</ul>
				</nav>
				<!-- <div class="description"><?php bloginfo('description'); ?></div> -->
			</header>