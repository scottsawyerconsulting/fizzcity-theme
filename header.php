<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="www-fizzcityfilms-com">
	<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<meta name="title" content="<?php
			  if (function_exists('is_tag') && is_tag()) {
				 single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
			  elseif (is_archive()) {
				 wp_title(''); echo ' Archive - '; }
			  elseif (is_search()) {
				 echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
			  elseif (!(is_404()) && (is_single()) || (is_page())) {
				 wp_title(''); echo ' - '; }
			  elseif (is_404()) {
				 echo 'Not Found - '; }
			  if (is_home()) {
				 bloginfo('name'); echo ' - '; bloginfo('description'); }
			  else {
				  bloginfo('name'); }
			  if ($paged>1) {
				 echo ' - page '. $paged; }
		   ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="google-site-verification" content="">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/lib/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/lib/img/apple-touch-icon.png">
        <meta property="og:title" content="Fizz City Films" />
        <meta property="og:url" content="<?php echo get_site_url(); ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="<?php echo get_site_url(); ?>/site-content/uploads/2018/05/Screen-Shot-2018-05-10-at-12.02.36-PM.png" />

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<script src="<?php bloginfo('template_directory'); ?>/lib/js/modernizr-2.5.2.min.js"></script>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>
	<script type="text/javascript" src="//use.typekit.net/mkt0grs.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

</head>

<body <?php body_class(); ?>>
		<div class="container">
			<header class="header" id="header">
				<div class="header-row-left">
					<div class="fizz-logo">
						<a href="<?php echo get_option('home'); ?>/" title="Home"><img src="/site-content/uploads/newlogo3.png"></a>
					</div>
				</div>
				<div class="header-row-right">
					<div class="social-nav">
						<a href="https://www.facebook.com/pages/Fizz-City-Films/173694972732911" target="_blank" />
							<img src="<?= get_template_directory_uri() . "/lib/images/facebook-icon.png" ?>" width="26" height="26" border="0" />
						</a>
					</div>
					<nav class="main-nav" role="navigation">
						<?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
					</nav>
				</div>
			</header>
			<div class="clearfix"></div>
			</div>
